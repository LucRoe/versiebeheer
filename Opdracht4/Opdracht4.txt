Studentnummer:
Naam:

Hoe open je de Terminal in Visual Studio Code?
	# CTRL + SHIFT + ` 

Met welk commando kan je checken of welke bestanden zijn toegevoegd aan de commit en welke niet?
	# git status

Wat is het commando van een multiline commit message?
	# git commit -m

Hoeveel commando's heb je in opdracht 4a uitgevoerd?
	# 13

Zoek het volgende commando op:
 - 1 commit teruggaan in de commit history. (reset)
	# git reset HEAD-1

